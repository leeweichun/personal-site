import './Notfound.css';
import Nav from './Nav.js';

function Notfound() {
  return (
    <div className="Notfound">
      <Nav />
      <div className="Content">
        <h1 className="four-oh-four">404</h1>
        <p className="four-oh-four-message">Whoops! Were you looking for something?</p>
      </div>
    </div>
    // --App  
  );
}

export default Notfound;
