import './Home.css';
import background from './assets/bg1.jpeg'
import Headshot from './assets/headshot.jpg';
import {Link} from 'react-router-dom';
import Nav from './Nav.js';

function Home() {
  return (
    <div className="Home" style={{backgroundImage:`url(${background})`}}>
      <Nav />
      <div className="container y mandatory-scroll-snapping" dir="ltr">
        <div>
          <div className="home-row-1">
            <div className="column left">
              <div className="Title">
                Clarence Lee
              </div>
              <div className="body-text-home">
                I'm a software engineer and graphic designer<br/> based in Canberra, Australia specializing in<br/> building bespoke websites and applications. 
                <br />
                <Link style={{color:"white", textDecoration:"none", marginLeft:"50px"}} to='/contact'>
                  <button className="button-contact"> Get in touch </button>
                </Link> 
                
              </div>
              
            </div>
            <div className="column right">
          
            </div>
          </div>
        </div>
        <div>
          <div className="home-row-2">
          <img className="headshot" src={Headshot} alt="Headshot" width="180" />
            <div className="nametitle">
              Hi, i'm Clarence Lee
            </div>
            <div className="body-text">
              I am a Software Engineer and Graphic Designer with a passion for functional design, efficiency, and visual refinement.
            </div>
          </div>
        </div>
        <div>
          <div className="home-row-3">
            <div className="column left">
              <div className="Title">
                Welcome to my personal site.
              </div>
            </div>
            <div className="column right">
          
            </div>
          </div>
        </div>
      </div>
    </div>
    // --App  
  );
}

export default Home;
