import './Nav.css';
import Sign from './assets/sign.png';
import {Link} from 'react-router-dom';

function Nav() {
  return (
    <div class="topnav">
      <Link style={{color:"white", textDecoration:"none"}} to='/'>
        <img src={Sign} width="100"></img>
      </Link>
      
      <div class="nav-links">
        <Link style={{color:"white", textDecoration:"none"}} to='/'>
          <p>Home</p>
        </Link>
        <Link style={{color:"white", textDecoration:"none", marginLeft:"50px"}} to='/experience'>
          <p>Experience</p>
        </Link> 
        <Link style={{color:"white", textDecoration:"none", marginLeft:"50px"}} to='/contact'>
          <p>Contact</p>
        </Link> 
      </div>
    </div>
   // --App  
  );
}

export default Nav;
