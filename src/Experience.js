import './Experience.css';
import Headshot from './assets/headshot.jpg';
import Nav from './Nav.js';

function Experience() {
  return (
    <div className="Experience">
      <Nav />
      <div className="container y mandatory-scroll-snapping" dir="ltr">
        <div>
          <div className="about-row-1">
            <img className="headshot" src={Headshot} alt="Headshot" width="180" />
            <div className="nametitle">
              Hi, i'm Clarence Lee
            </div>
            <div className="body-text">
              I am a Software Engineer and Graphic Designer with a passion for functional design, efficiency, and visual refinement.
            </div>
          </div>
        </div>
        <div>
          <div className="about-row-2">
            <img className="headshot" src={Headshot} alt="Headshot" width="200" />
            <div className="nametitle">
              It's Clarence!
            </div>
          </div>
        </div>
        <div>
          <div className="about-row-3">
            <img className="headshot" src={Headshot} alt="Headshot" width="200" />
            <div className="nametitle">
              It's Clarence!
            </div>
          </div>
        </div>
      </div>
    </div>
   // --App  
  );
}

export default Experience;
