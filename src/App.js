import Home from './Home.js';
import Experience from './Experience.js';
import Contact from './Contact.js';
import Notfound from './Notfound.js';
import './App.css';
import {BrowserRouter as Router, Switch, Route} from "react-router-dom"

function App() {
  return (
    <Router>
      <div className="App">
          <Switch>
            <Route exact path="/experience" component={Experience} />
            <Route exact path="/contact" component={Contact} />
            <Route exact path="/" component={Home} />
            <Route component={Notfound} />
          </Switch>
      </div>
    </Router>
  );
}

export default App;
